# IOC for DTL-010 vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   DTL-010:Vac-VEG-10001
    *   DTL-010:Vac-VGP-10000
    *   DTL-010:Vac-VGC-10000
    *   DTL-010:Vac-VGC-50000
